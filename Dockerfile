FROM nvidia/cuda:12.0.0-runtime-ubuntu22.04
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,video,utility
RUN mkdir /usr/src/IODA
WORKDIR /usr/src/IODA
ADD src/IODA /usr/src/IODA/src/IODA
COPY pyproject.toml /usr/src/IODA/pyproject.toml
COPY README.md /usr/src/IODA/README.md
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && apt-get install -y software-properties-common && \
    add-apt-repository ppa:deadsnakes/ppa && \
    apt-get install --no-install-recommends -y \
    build-essential gcc curl ca-certificates \
    python3.9-minimal python3.9-venv \
    python3-cachecontrol python3-poetry \
    ffmpeg && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
RUN poetry env use python3.9
RUN poetry install