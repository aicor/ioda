import os

import torch
from torch.distributed.elastic.multiprocessing.errors import record
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.tensorboard import SummaryWriter

from IODA.args import parse_validation
from IODA.initialize import initialize
from IODA.resume import resume_if_possible
from IODA.train import train
from IODA.validate import validate


@record
def main():
    # Important: For this configuration to work, the script needs to be spawned by torchrun!
    # For command line arguments, see args.py
    torch.distributed.init_process_group(backend="nccl")

    args = parse_validation()
    local_rank = int(os.environ["LOCAL_RANK"])
    log = SummaryWriter()

    model, optimizer, loss_fn, image_size = initialize(args)
    model = DDP(model.to(local_rank), device_ids=[local_rank], output_device=local_rank)
    loss_fn, epoch = resume_if_possible(model, optimizer, loss_fn)
    loss_fn.to(local_rank)
    train(model, optimizer, loss_fn, epoch, image_size, args, log)
    validate(model, image_size, args, log)


if __name__ == "__main__":
    main()
