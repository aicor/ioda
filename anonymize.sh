#!/bin/bash

BATCH_SIZE=32 # Currently batches are used as sequences
SEQUENCE_LENGTH=1
STRIDE=1   # Distance between consecutive frames in the sequence
STEP=5     # Step between sequences
WORKERS=12 # Threads per gpu
MOUNT_FOLDER=/path/to/folder # Mounted in docker, contains video files, results are also saved here
INPUT="test_video.mp4" # Input video
OUTPUT="test_video_anon.mp4" # Anonymized video
NO_GPUS=1 # Number of gpus to use

# Using docker to run anonymization
# Needs docker and nvidia-docker2 installed
# https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#install-guide
# Replace the mounted volume ("$absolutePath/toLocalFolder") and
# the input/output video filename ("$i/o_video.mp4") by your local files
docker run --runtime=nvidia --gpus "$NO_GPUS" \
    -v "$MOUNT_FOLDER":/usr/IODA_anonymization \
    -t a0schulze/ioda \
    poetry run torchrun --standalone --nnodes=1 --nproc_per_node="$NO_GPUS" --max_restarts=3 src/IODA/anonymize_video.py \
    --i="/usr/IODA_anonymization/$INPUT" --o="/usr/IODA_anonymization/$OUTPUT" \
    --model="src/IODA/IODA_model.pt" \
    --batch-size="$BATCH_SIZE" --sequence-length="$SEQUENCE_LENGTH" --stride="$STRIDE" \
    --step="$STEP" --num-classes=2 --workers="$WORKERS"
