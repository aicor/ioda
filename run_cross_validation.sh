#!/bin/bash
# This script will run the 5-fold cross-validation experiments.
# Each split is run on a single gpu while utilizing all available gpus.
# Since this script will take a long time to execute we recommend using nohup
# to run the script.

BATCH_SIZE=32 # Currently batches are used as sequences
SEQUENCE_LENGTH=1
STRIDE=1   # Distance between consecutive frames in the sequence
STEP=5     # Step between sequences
EPOCHS=5   # Number of epochs for training
WORKERS=12 # Threads per gpu

experiments=(
    # Experiment 1 Balanced weights, binary classes
    "results/binary/split_1,results/binary/split_1/model.pt,data/binary/train_1.txt,data/binary/val_1.txt,2,1,1"
    "results/binary/split_2,results/binary/split_2/model.pt,data/binary/train_2.txt,data/binary/val_2.txt,2,1,1"
    "results/binary/split_3,results/binary/split_3/model.pt,data/binary/train_3.txt,data/binary/val_3.txt,2,1,1"
    "results/binary/split_4,results/binary/split_4/model.pt,data/binary/train_4.txt,data/binary/val_4.txt,2,1,1"
    "results/binary/split_5,results/binary/split_5/model.pt,data/binary/train_5.txt,data/binary/val_5.txt,2,1,1"
    # Experiment 2 Balanced weights, non binary classes
    "results/multiclass/split_1,results/multiclass/split_1/model.pt,data/multiclass/train_1.txt,data/multiclass/val_1.txt,5,1,1,1,1,1"
    "results/multiclass/split_2,results/multiclass/split_2/model.pt,data/multiclass/train_2.txt,data/multiclass/val_2.txt,5,1,1,1,1,1"
    "results/multiclass/split_3,results/multiclass/split_3/model.pt,data/multiclass/train_3.txt,data/multiclass/val_3.txt,5,1,1,1,1,1"
    "results/multiclass/split_4,results/multiclass/split_4/model.pt,data/multiclass/train_4.txt,data/multiclass/val_4.txt,5,1,1,1,1,1"
    "results/multiclass/split_5,results/multiclass/split_5/model.pt,data/multiclass/train_5.txt,data/multiclass/val_5.txt,5,1,1,1,1,1"
)

benchmark_set=(
    "results/benchmark,results/benchmark/model.pt,data/binary/file_list_binary.txt,data/binary/file_list_binary.txt,2,1,1"
)

get_gpu_count() {
    printf "Getting number of available gpus\n"
    local -n ref=$1
    ref=$(lspci | grep -i nvidia | grep -e VGA -e 3D | wc -l)
    if [ $ref == 0 ]; then
        printf "No gpus available\n"
        printf "Exiting\n"
        exit
    fi
    printf "Using $ref gpus\n\n"
}

parallel_and_torchrun_available() {
    if ! command -v parallel &>/dev/null; then
        printf "GNU parallel could not be found\n"
        printf "On Ubuntu, you can install it by sudo apt install parallel\n"
        exit
    fi

    if ! command -v torchrun &>/dev/null; then
        printf "torchrun could not be found\n"
        printf "Install python 3.9 first and then install the dependencies from\
        pyproject.toml. This can be done with poetry for example.\n"
        exit
    fi
}

cross_validation_single_thread() {
    IFS="," read -r -a arr <<<$1 # Reads one line of array experiments
    echo -e "Starting job on gpu $(($2 - 1)) with the following parameters\n\
output folder: ${arr[0]}\nmodel checkpoint: ${arr[1]}\n\
training list: ${arr[2]}\nvalidation list: ${arr[3]}\nclass number: ${arr[4]}\n\
class weights: ${arr[@]:5}\n"

    # The stateful LSTM layer can not be distributed! Hence, we only make one gpu visible.
    CUDA_VISIBLE_DEVICES=$(($2 - 1)) \
        torchrun --standalone --nnodes=1 --nproc_per_node=1 --max_restarts=10 main.py \
        --out="${arr[0]}" --checkpoint="${arr[1]}" --train="${arr[2]}" --val="${arr[3]}" \
        --transfer-learning --class-weights "${arr[@]:5}" --num-classes="${arr[4]}" \
        --batch-size="$BATCH_SIZE" --sequence-length="$SEQUENCE_LENGTH" --stride="$STRIDE" \
        --step="$STEP" --epochs="$EPOCHS" --workers="$WORKERS"

    echo -e "Finishing $1 on gpu $(($2 - 1))\n"
}

cross_validation_in_parallel() {
    gpu_count=0
    get_gpu_count gpu_count

    parallel_and_torchrun_available

    export BATCH_SIZE
    export SEQUENCE_LENGTH
    export STRIDE
    export STEP
    export EPOCHS
    export WORKERS
    export -f cross_validation_single_thread
    parallel --line-buffer -j$gpu_count cross_validation_single_thread {} {%} ::: ${experiments[@]}
}

cross_validation() {
    gpu_count=0
    get_gpu_count gpu_count

    for tuple in "${experiments[@]}"; do
        IFS="," read -r -a arr <<<$tuple # Reads one line of array experiments
        echo -e "Starting job with the following parameters\n\
output folder: ${arr[0]}\nmodel checkpoint: ${arr[1]}\n\
training list: ${arr[2]}\nvalidation list: ${arr[3]}\nclass number: ${arr[4]}\n\
class weights: ${arr[@]:5}\n"

        torchrun --standalone --nnodes=1 --nproc_per_node=$gpu_count --max_restarts=3 main.py \
            --out="${arr[0]}" --checkpoint="${arr[1]}" --train="${arr[2]}" --val="${arr[3]}" \
            --transfer-learning --class-weights "${arr[@]:5}" --num-classes="${arr[4]}" \
            --batch-size="$BATCH_SIZE" --sequence-length="$SEQUENCE_LENGTH" --stride="$STRIDE" \
            --step="$STEP" --epochs="$EPOCHS" --workers="$WORKERS"
    done
}

benchmark() {
    gpu_count=0
    get_gpu_count gpu_count

    for tuple in "${benchmark_set[@]}"; do
        IFS="," read -r -a arr <<<$tuple # Reads one line of array
        echo -e "Starting job with the following parameters\n\
output folder: ${arr[0]}\nmodel checkpoint: ${arr[1]}\n\
training list: ${arr[2]}\nvalidation list: ${arr[3]}\nclass number: ${arr[4]}\n\
class weights: ${arr[@]:5}\n"

        torchrun --standalone --nnodes=1 --nproc_per_node=$gpu_count --max_restarts=3 main.py \
            --out="${arr[0]}" --checkpoint="${arr[1]}" --train="${arr[2]}" --val="${arr[3]}" \
            --transfer-learning --class-weights "${arr[@]:5}" --num-classes="${arr[4]}" \
            --batch-size="$BATCH_SIZE" --sequence-length="$SEQUENCE_LENGTH" --stride="$STRIDE" \
            --step="$STEP" --epochs="$EPOCHS" --workers="$WORKERS"
    done
}

main() {
    cross_validation # Default: #gpu_count gpus per experiment and 1 experiment at a time
    # cross_validation_in_parallel # One gpu per experiment and #gpu_count experiments in parallel
    # benchmark # Benchmark: train and test on whole data set
}

main
